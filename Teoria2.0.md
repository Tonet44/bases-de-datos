

Antonio Esteban Lorenzo / 1DAW Bases de datos

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/blob/main/Teoria2.0.md

## MEMORIA

Podemos trabajar con Mysql desde el interfaz gráfico o desde "El interfaz de linea de comandos" es decir el terminal.
Para poder trabajar desde el terminal hay que  levantar el servidor, en nuestro caso poner en marcha docker e iniciar sesión de la siguiente manera:

```sql
mysql -u root -p -h 127.0.0.1 -P 33006
```

Una vez puesto este comando nos pedirá la contraseña, que en nuestro caso es "dbrootpass".

Desde el interfaz de linea de comandos se puede realizar cualquier consulta, crear tablas, cambiar datos, eliminar datos...

Vamos a ver varios ejemplos de como se puede trabajar con el.

Ver tablas de una Bd

```sql
SHOW TABLES FROM Chinook;
SHOW FULL TABLES FROM Chinook;
SHOW SCHEMAS;
```

Ver todas las Bd

```sql
SHOW SCHEMAS;
```

Ver una tabla.

```sql
SHOW COLUMNS FROM Employee;
DESCRIBE Employee;
```

Te dice en que Bd de datos estas.

```sql
SELECT DATABASE();
```

### ACID

Transacción Atomica, esto es como una norma o ley que tienen todos los sistemas para mantener una integridad a la hora de hacer una transacción de datos. Por ejemplo si vas a la gasolinera y pagas la gasolina que te vas a poner con tarjeta y surge un fallo en el sistema y no te puedes poner gasolina, veras en tu cuenta que no se hizo la transacción, porque tiene que completarse todo el proceso para que se complete.

### (TCL) COMMIT Y AUTOCOMMIT

Para guaradar datos en nuestra Bd de datos lo podemos hacer de forma automática con el "autocommit", o de forma manual con el "commit".
Esto se puede cambiar de la siguiente forma:

```sql
SET autocommit=0;
SET autocommit=1;
```

Con el "1" lo tendremos activado y con el "0" desactivado.
Si lo tenemos en autocommit cada movimiento que hagamos se guardara automáticamente, en cambio con el commit tienes que guardar tu mismo escribiendo "COMMIT", si lo tienes desactivado y no realizas un commit no se guardaran los cambios realizados.

Para ver si lo tenemos en auto o no, ponemos el comnado siguiente:

```sql
SELECT @@autocommit;
```

### (TCL) ROLLBACK

El "Rollback" sirve para deshacer los cambios realizados hasta el último commit, solo se pueden deshacer los cambios del último commit siempre y cuando lo tengas en modo manual, si lo tienes en modo automático este metodo no se puede utilizar.

**EJEMPLO DE LO EXPLICADO HASTA EL MOMENTO**

```sql
COMMIT;
USE Empresa;
SHOW TABLES;
DESCRIBE Comanda;
SELECT * FROM Comanda;
INSERT INTO Comanda VALUES (622, CURRENT-DATE(), 'A', 100, NULL,100);
SELECT * FROM Comanda;
ROLLBACK;
SELECT * FROM Comanda;
```

En este ejemplo he usado el interfaz del terminal para realizar un commit antes de realizar una inserccion en una tabla, después de haber introducido datos en la tabla Comanda, hago un Rollback y vuelve al commit que hicimos al principio.

**CREAR BD Y TABLAS DESDE EL INTERFAZ DEL WORKBENCH**

En esta base de datos que he creado he utilizado tres tablas y con ello consigo varias cosas, puedo tener los datos de los clientes por una parte, los datos de los vuelos por otra y luego puedo relacionarlos con la tabla vuelos_pasajeros. Teniendo una bd más eficiente, porque cuando hay relaciones de uno a muchos puede ser bastante lioso guardar información en una sola tabla y poder relacionarlo.

```sql
CREATE DATABASE `Eje_Vuelo`;

USE `Eje_Vuelo`;

CREATE TABLE `vuelos`
(
    `id_vuelos` INT AUTO_INCREMENT NOT NULL,
    `numero_vuelo` VARCHAR(30) NOT NULL,
    `origen` VARCHAR(30) NOT NULL,
    `destino` VARCHAR(30) NOT NULL,
    `fecha` DATE NOT NULL,
    `capacidad` INT NOT NULL,  
	PRIMARY KEY (id_vuelos)
    
);

CREATE TABLE `pasajeros`
(
    `id_pasajero` INT AUTO_INCREMENT NOT NULL,
    `numero_pasaporte` VARCHAR(30) NOT NULL,
    `nombre_pasajero` VARCHAR(30) NOT NULL,
    PRIMARY KEY (id_pasajero)
	
);

CREATE TABLE `vuelos_pasajeros`
(
    `id_vuelo` INT NOT NULL,
    `id_pasajero` INT NOT NULL,
    `n_asiento` INT NOT NULL,
    PRIMARY KEY (id_vuelo, id_pasajero),
    FOREIGN KEY (id_vuelo) REFERENCES vuelos(id_vuelos),
    FOREIGN KEY (id_pasajero) REFERENCES pasajeros(id_pasajero)
	
);
```

En este caso que acabo de poner he creado una bd llamada "vuelos" y tres tablas llamadas "vuelos, pasajeros y vuelos_pasjeros" con el `CREATE TABLE`.
Luego a cada columna le puse una serie de restricciones. La restricciones se ponen acorde a lo que se necesite tratar, luego siempre se pueden modificar, añadiendo o eliminando, yo en este caso he usado 
las siguientes:

- NOT NULL: No se pude quedar este campo vacío al insertar datos.

- NULL: Si no se inserta campo en esa columna saldra como null.

- AUTO_INCREMENTAL: Cuando insertemos datos se pongan de forma automática sin necesidad de poner nosotros nada.

- INT: solo puedes utilizar números entreos en esa columna.

- VARCHAR(30): Solo puedes usar cadenas de texto de hasta 30 caracteres.

- PRIMARY KEY: Es la clave primaria, es única y es la que nos servirá para relacionarnos con otras tablas.

- FOREING KEY: Es la clave foranea, esta servirá pra relacionar tablas de 1 a muchos.

Si no tuviesemos claves foraneas y primarias la integridad de la bd se perdería. Podrias reservar un vuelo con los datos de un cliente que no existe, y eso seria un grave problema.

Hay muchos mas tipos de restricciones, como:

- CHECK: Sirve para poner una condición antes de insertar datos.

- DEFAULT: Le puedes poner un dato por defecto, y si al insertar datos lo dejan vacío se pondra el dato por defecto.

- UNIQUE: Solo puede haber uno en toda la tabla, como el dni.

- GENERATED: Se utiliza para reforzar el UNIQUE, generando una condición, lo explico con un ejemplo. El DNI puede ser un UNIQUE pero puede dar la casualidad que dos personas de distinto País tengan el mismo documento, con el Generated creamos una condición que para que puedas insertar el DNI tiene que ser de distinto Pais al que ya lo tiene insertado.

**ALTER TABLE**

Añadir comlumna llamada "DNI", se añade despues de la columna de nombre_pasajero. Y si no se insertan datos se queda null
Añadir columna "nacionalidad" es DEFAULT 'ESP' y se añade despues de la columna DNI.

```sql
ALTER TABLE `pasajeros` 
ADD COLUMN `DNI` VARCHAR(15) NULL AFTER `nombre_pasajero`,
ADD COLUMN `nacionalidad` VARCHAR(20) NULL DEFAULT 'ESP' AFTER `DNI`,
```

La columna de "DNI" la hago UNIQUE.

```sql
ALTER TABLE `eje_vuelo`.`pasajeros` 
ADD UNIQUE INDEX `DNI_UNIQUE` (`DNI` ASC);
```

Cambio la columna DNI a NOT NULL.
Cambio la columna nacionalidad a INT y NOT NULL

```sql
ALTER TABLE `eje_vuelo`.`pasajeros` 
CHANGE COLUMN `DNI` `DNI` VARCHAR(15) NOT NULL ,
CHANGE COLUMN `nacionalidad` `nacionalidad` INT NOT NULL ;
```

Eliminar columna nacionalidad

```sql
ALTER TABLE `eje_vuelo`.`pasajeros` 
DROP COLUMN `nacionalidad`,
```

## PARTE 2

### CREAR USUARIOS

CREAR NUEVO USUARIO

```sql
CREATE USER 'antonio'@'localhost' IDENTIFIED BY '1234';
```

CREAR NUEVO USUARIO SIN CONTRASEÑA

```sql
CREATE USER 'nuevo_usuario'@'localhost';
```

**RENOMBRAR USUARIO**

```sql
RENAME USER 'nuevo_usuario'@'localhost' TO 'nuevo_nombre'@'localhost';
```

**MODIFICAR USUARIOS**

CAMBIAR LA CONTRASEÑA DE UN USUARIO EXISTENTE

```sql
ALTER USER 'nuevo_usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña_segura';
```
CAMBIAR EL HOST DESDE EL CUAL UN USUARIO PUEDE CONECTARSE

```sql
RENAME USER 'nuevo_usuario'@'localhost' TO 'nuevo_usuario'@'%' ;
```

**PRIVILEGIOS**

DAR PRIVILEGIOS ESPECIFICOS A UN USUARIO

```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON base_de_datos.* TO 'nuevo_usuario'@'localhost';
```

DAR PRIVILEGIOS DE TODO A UN USUARIO

```sql
GRANT ALL PRIVILEGES ON nombre_base_datos.* TO 'nuevo_usuario'@'localhost';
```

PARA APLICAR LOS CAMBIOS REALIZADOS EN LOS PRIVILEGIOS SIEMPRE APLICAR

```sql
FLUSH PRIVILEGES;
```

**ELIMINAR USUARIOS**

```sql
DROP USER 'nuevo_usuario'@'localhost';
```

**MOSTRAR PERMISOS ASOCIADOS A CADA USUARIO**

```sql
SHOW GRANTS FOR 'username'@'host';
```

**MOSTRAR LOS USUARIOS**

```sql
Mysql> SELECT user,host FROM mysql.user;
```

### ROLES

**CREAR ROL**

```sql
CREATE ROLE 'mi_rol';
```

**ASIGNAR PRIVILEGIOS AL ROL**

```sql
GRANT SELECT, INSERT, UPDATE ON mi_base_de_datos.mi_tabla TO 'mi_rol';
```

**ASIGNAR EL ROL AL USUARIO**

```sql
GRANT 'mi_rol' TO 'mi_usuario'@'localhost';
```

**ACTIVAR EL ROL AUTOMÁTICAMENTE CUANDO EL USUARIO INICIE SESION POR DEFECTO**

```sql
ALTER USER 'mi_usuario'@'localhost' DEFAULT ROLE 'mi_rol';
```

**SI NO SE ACTIVA DE FORMA AUTO HAY QUE PONER EL SIGUIENTE COMANDO AL ENTRAR**

```sql
SET ROLE 'mi_rol';
```

**ELIMINAR UN ROL A UN USUARIO**

```sql
REVOKE 'mi_rol' FROM 'mi_usuario'@'localhost';
```

**ELIMANAR UN ROL**

```sql
DROP ROLE 'mi_rol'
```

**Para conectarme a la Bd de mi compañero, necesito el usuario, la Ip, el puerto y la contraseña.**

```sql
 mysql -u root -p -h 192.168.5.4 -P 33006
```

### COMO FUNCIONAN LOS PRIVILEGIOS EN MYSQL

MySQL asegura que todos los usuarios pueden ejecutar sólo la operación permitida a los mismos. Como usuario, cuando te conectas a un servidor MySQL, su identidad se determina mediante el equipo desde el que se conecta y el nombre de usuario que especifique.Cuando efectúe peticiones tras conectar, el sistema le otorga privilegios acorde a su identidad y lo que quiera hacer.
El servidor guarda información de privilegios en las tablas de permisos de la base de datos mysql (esto es, en la base de datos llamada mysql).
Normalmente, manipula los contenidos de las tablas de permisos indirectamente usando los comandos `GRANT` y `REVOKE` para configurar cuentas y controlar los privilegios disponibles para cada una.
Cuando modificamos los contenidos de las tablas de permisos, es una buena idea asegurarse que sus cambios configuran permisos tal y como desea. Para consultar los permisos de una cuenta dada, use el comando `SHOW GRANTS`.

### CUANDO TIENEN EFECTO EL CAMBIO DE PRIVILEGIOS

Si modificamos las tablas grant utilizando `GRANT, REVOKE, o SET PASSWORD`,el servidor se da cuenta de estos cambios y recarga las tablas grant en la memoria inmediatamente.

Si modificamos las tablas grant directamente utilizando sentencias como `INSERT, UPDATE, o DELETE`, los cambios no tendrán efecto en la comprobación de privilegios hasta que se reinicie el servidor, o bien se le comunique a éste que debe recargar las tablas. Para recargar las tablas manualmente, ejecute la sentencia `FLUSH PRIVILEGES` o los comandos mysqladmin flush-privileges o mysqladmin reload.

### ¿QUE ES EL CONTROL DE ACCESO EN MYSQL?

El control de acceso de MySQL implica dos etapas: 
- Etapa 1: El servidor comprueba si debe permitirle conectarse. 
- Etapa 2: Asumiendo que se conecta, el servidor comprueba cada comando que ejecuta para ver si tiene suficientes permisos para hacerlo.

### AUTENTICACIÓN DE USUARIOS

En MySQL, la autenticación de usuarios se realiza mediante un mecanismo de autenticación basado en credenciales, que verifica la identidad de un usuario antes de permitirle acceder a la base de datos.

El proceso de autenticación en MySQL se lleva a cabo de la siguiente manera:

- Un usuario intenta conectarse a la base de datos proporcionando un nombre de usuario y una contraseña.

- El servidor MySQL verifica si el nombre de usuario y la contraseña proporcionados coinciden con los almacenados en la tabla mysql.user.

- Si la combinación de nombre de usuario y contraseña es válida, el servidor MySQL autentica al usuario y le otorga acceso a la base de datos.

MySQL ofrece varias opciones de autenticación, que se pueden configurar según las necesidades de seguridad y autenticación de cada organización:

- **Autenticación estándar:** Esta es la opción de autenticación predeterminada en MySQL. El servidor MySQL almacena las contraseñas de los usuarios en la tabla mysql.user y las compara con las proporcionadas por el usuario que intenta conectarse.

- **Autenticación con SSL/TLS:** Esta opción permite autenticar a los usuarios mediante certificados digitales SSL/TLS. Los usuarios deben proporcionar un certificado digital válido para conectarse a la base de datos.

- **Autenticación con PAM (Pluggable Authentication Modules):** Esta opción permite integrar MySQL con sistemas de autenticación externos, como LDAP o Active Directory, utilizando módulos de autenticación PAM.

- **Autenticación con Kerberos:** Esta opción permite autenticar a los usuarios mediante el protocolo de autenticación Kerberos.

- **Autenticación con caching_sha2_password:** Esta opción utiliza un algoritmo de hash más seguro que el algoritmo de hash predeterminado en MySQL. Esta opción es compatible con la autenticación con SSL/TLS.

- **Autenticación con ed25519:** Esta opción utiliza el algoritmo de firma digital Ed25519 para autenticar a los usuarios.

### TIPOS DE PERMISOS Y GRANULARIDAD
En MySQL, un usuario puede tener diferentes tipos de permisos, cada uno con diferentes niveles de granularidad. Los permisos se asignan a un usuario específico y se pueden limitar a un esquema de base de datos o una tabla específica.

TIPOS DE PERMISOS:
- **SELECT:** Permite al usuario seleccionar datos de una tabla o vista.

- **INSERT:** Permite al usuario insertar datos en una tabla.

- **UPDATE:** Permite al usuario actualizar datos en una tabla.

- **DELETE:** Permite al usuario eliminar datos de una tabla.

- **CREATE:** Permite al usuario crear nuevas tablas o bases de datos.

- **DROP:** Permite al usuario eliminar tablas o bases de datos existentes.

- **ALTER:** Permite al usuario alterar la estructura de una tabla.

- **INDEX:** Permite al usuario crear o eliminar índices en una tabla.

- **GRANT OPTION:** Permite al usuario conceder o revocar permisos a otros usuarios.

- **PROCESS:** Permite al usuario ver los procesos de MySQL en ejecución.

- **RELOAD:** Permite al usuario recargar los archivos de configuración de MySQL.

- **SHUTDOWN:** Permite al usuario detener el servidor MySQL.

- **SUPER:** Permite al usuario superar algunas restricciones de MySQL.

Además de estos permisos, MySQL también ofrece una granularidad adicional mediante el uso de restricciones de privilegios a nivel de columna y fila. Estas restricciones permiten al administrador de la base de datos restringir el acceso a columnas específicas o filas específicas de una tabla.


## BIBLIOGRAFÍA

http://download.nust.na/pub6/mysql/doc/refman/5.0/es/privilege-changes.html

http://download.nust.na/pub6/mysql/doc/refman/5.0/es/privileges.html#:~:text=El%20control%20de%20acceso%20de,tiene%20suficientes%20permisos%20para%20hacerlo.

https://www.hostinger.es/tutoriales/como-crear-usuario-mysql

https://dev.mysql.com/doc/refman/8.3/en/access-control.html













