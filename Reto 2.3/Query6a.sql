#Query-6/Reto2.3/Obtener los álbumes con un número de canciones superiores a la media.

SELECT *
FROM Album;

SELECT *
FROM Track;

SELECT AVG(T.TrackId) as Media_canciones
FROM Track AS T
GROUP BY T.AlbumId;

SELECT AlbumId, Title
FROM Album
WHERE AlbumId in ( SELECT AVG(T.TrackId) as Media_canciones
FROM Track AS T
GROUP BY T.AlbumId);

-- CRISTIAN
-- MEDIA DE CANCIONES EN TODOS LOS ALBUMES
SELECT AVG(N_Canciones) FROM
(
	SELECT AlbumId, COUNT(*) AS N_Canciones
    FROM Track
    GROUP BY AlbumId
    ) AS Album_Ncanciones;
    
-- AlbumId, junto al número de canciones

SELECT AlbumId, COUNT(*) AS N_Canciones
    FROM Track
    GROUP BY AlbumId
    HAVING N_Canciones > (
		SELECT AVG(N_Canciones) FROM
		(
			SELECT AlbumId, COUNT(*) AS N_Canciones
			FROM Track
			GROUP BY AlbumId
		) AS Album_Ncanciones
	);
    
