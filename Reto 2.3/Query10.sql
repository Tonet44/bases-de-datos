#Query-10/Reto2.3/Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

#Consulta, suma total compras por cliente
USE Chinook;
SELECT CustomerId, SUM(Total) as total_compras
FROM invoice
GROUP BY CustomerId;

#Bien hecha con Join
USE Chinook;
SELECT C.FirstName, C.LastName, I.CustomerId, SUM(I.Total) as total_compras
FROM Customer AS C
JOIN Invoice AS I ON C.CustomerId = I.CustomerId
GROUP BY I.CustomerId;

#Con subconsulta
USE Chinook;
SELECT C.FirstName, C.LastName, C.CustomerId,
    (SELECT SUM(I.Total) FROM Invoice AS I WHERE I.CustomerId = C.CustomerId) AS total_compras
FROM Customer AS C;

-- 02/06/24

SELECT *
FROM Customer;

SELECT *
FROM invoice;


SELECT CustomerId, SUM(Total) as total_por_cliente
FROM invoice
GROUP BY CustomerId;

SELECT concat(C.FirstName," ",C.LastName) AS Nombre_cliente, SUM(I.Total) as total_por_cliente
FROM customer AS C JOIN invoice AS I ON C.CustomerId = I.CustomerId
GROUP BY I.CustomerId
ORDER BY total_por_cliente DESC;


