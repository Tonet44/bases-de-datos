#Query-9/Reto2.3/Canciones de la playlist con más canciones

USE Chinook;
SELECT p.PlaylistId, COUNT(*)
FROM track as t
JOIN Playlisttrack AS p
ON t.TrackId = p.TrackId
GROUP BY t.TrackId;

-- 02/06/24

SELECT *
FROM Track;

SELECT *
FROM playlist;

select *
from playlisttrack;

select *
from playlisttrack
WHERE PlaylistId = 3;

select PlaylistId, count(*) AS Canciones_de_cada_playlist
from playlisttrack
GROUP BY PlaylistId
order by Canciones_de_cada_playlist DESC
LIMIT 1;
                
SELECT PT.PlaylistId, T.TrackId, T.Name
FROM Track AS T
JOIN playlisttrack AS PT ON T.TrackId = PT.TrackId
WHERE PT.PlaylistId = (
    SELECT PlaylistId
    FROM playlisttrack
    GROUP BY PlaylistId
    ORDER BY COUNT(*) DESC
    LIMIT 1
);