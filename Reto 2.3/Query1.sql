#Query-1/Reto2.3/Obtener las canciones con una duración superior a la media.
USE chinook;
SELECT *
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track);
