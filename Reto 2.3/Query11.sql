#Query-11/Reto2.3/Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

USE Chinook;
SELECT SupportRepId, COUNT(DISTINCT CustomerId)
FROM Customer
GROUP BY SupportRepId;

#Con subconsultas
USE Chinook;
SELECT E.EmployeeId, E.FirstName, E.LastName,
(SELECT COUNT(DISTINCT C.CustomerId) FROM Customer AS C WHERE C.SupportRepId = E.EmployeeId GROUP BY C.SupportRepId) AS Total_clientes
FROM Employee AS E;

#Con join
USE Chinook;
SELECT E.EmployeeId, E.FirstName, E.LastName, COUNT(DISTINCT C.CustomerId) AS Total_clientes
FROM Employee AS E
JOIN  Customer AS C ON C.SupportRepId = E.EmployeeId
GROUP BY C.SupportRepId;

