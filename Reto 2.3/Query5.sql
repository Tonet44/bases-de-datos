#Query-5/Reto2.3/Álbumes que tienen más de 15 canciones, junto a su artista.
USE Chinook;
SELECT T.AlbumId, T.Name, COUNT(T.TrackId) as Numer_canciones, AR.Name AS Nombre_Artista
FROM Track AS T
JOIN Album AS A ON T.AlbumId = A.AlbumId
JOIN Artist AS AR ON A.ArtistId = AR.ArtistId
GROUP BY T.AlbumId
HAVING COUNT(T.TrackId) > 15;

SELECT
  A.AlbumId,
  AR.Name AS ArtistName,
  A.Title AS AlbumName,
  COUNT(T.TrackId) AS NumberOfTracks
FROM
  Album AS A
  JOIN Artist AS AR ON A.ArtistId = AR.ArtistId
  JOIN Track AS T ON T.AlbumId = A.AlbumId
GROUP BY
  A.AlbumId,
  AR.Name,
  A.Title
HAVING
  COUNT(T.TrackId) > 15
LIMIT 0, 1000;

SELECT * FROM Album JOIN Artist USING (ArtistId)
WHERE AlbumId IN (
	SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > 15
);

# 27/05/24

SELECT *
FROM Album;

SELECT *
FROM Track;

SELECT *
FROM artist;

SELECT  AlbumId, COUNT(AlbumId) as sum
FROM Track
GROUP BY AlbumId
HAVING sum > 15;


SELECT T.AlbumId, A.Name AS Artist, AL.Title AS Album
FROM artist AS A
JOIN Album AS AL ON A.ArtistId = AL.ArtistId
JOIN (
    SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > 15
) AS T ON AL.AlbumId = T.AlbumId;

-- 03/06/24

SELECT *
FROM Track;

SELECT AlbumId, TrackId, Name, COUNT(TrackId) AS Total_canciones
FROM Track
GROUP BY AlbumId
HAVING Total_canciones > 15
Order by Total_canciones DESC;

SELECT T.AlbumId, A.Name AS Nombre_Album, T.Name AS Nombre_Cancion, COUNT(T.TrackId) AS Total_canciones
FROM Track AS T JOIN Album AS AL ON T.AlbumId = AL.AlbumId
JOIN Artist AS A On AL.ArtistId = A.ArtistId
GROUP BY T.AlbumId
HAVING Total_canciones > 15
Order by T.AlbumId DESC;