Antonio Esteban Lorenzo.
1 DAW.Bases de datos

## COMANDOS

## SANITAT
**1. Muestre los hospitales existentes (número, nombre y teléfono).**

```sql
SELECT 
HOSPITAL_COD AS "Código",
NOM,
TELEFON 
FROM HOSPITAL;
```
**2. Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.**

```sql
SELECT *
FROM HOSPITAL
WHERE NOM LIKE '_A%';
```
En la tabla `HOSPITAL` y en la columna `NOM` filtramos con el `LIKE` para buscar la letra "A".
El "_A" dice que buscas una palabra cuya segunda letra sea la A, si fuese con dos barras bajas sería con la tercera letra, "__A"
"%" Significa cualquier letra detras de la "A", si no pusieramos nada estaríamos buscando una palabra de dos letras y que la segunda fuese la "A".
Si fuese "%a% buscariamos todas las palabra que tuviesen una a en la palabara.

OTRAS FORMAS DE HACERLO

```sql
SELECT *
FROM HOSPITAL
WHERE HOSPITAL.NOM LIKE  '_a%';
```
Es como el anterior pero remarcando que `NOM` pretenece a la tabla `HOSPITAL`

```sql
SELECT *
FROM HOSPITAL
WHERE substr(NOM, 2, 1) = "a";
```
Esta opción es con subString, es mas compleja
EL "2" es la posición de la letra y el "1" es la cantidad de cararcteres que coge, se supone que estamos buscando una palabra que la segunda letra sea la "a".

```sql
SELECT NOM, SUBSTR(NOM, 2, 4)
FROM HOSPITAL
WHERE substr(NOM, 2, 1) = "a";
```
```sql
SELECT NOM AS "Nombre",
 SUBSTR(NOM, 1, 2) AS "Parte seleccionada"
FROM HOSPITAL
WHERE substr(NOM, 1, 2) = "la";
```
**3. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.**

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```
**4. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.**

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM, TORN
FROM PLANTILLA
WHERE TORN IN ('M','T');
```
Con el `IN` estamos bucando las que sean "M" y "T", que son las otras dos opciones que hay en esa columna aparte de la de "N".

OTRAS OPCIONES
```sql
WHERE TORN != "N"; 
WHERE TORN <> "N";
WHERE NOT TORN = "N";
WHERE TORN = "M" OR TORN = "T";
```

**5. Muestre a los enfermos nacidos en 1960.**
```sql
SELECT *
FROM MALALT
WHERE DATA_NAIX >= '1960-01-01' AND DATA_NAIX <='1960-12-31';
```
OTRAS OPCIONES
```sql
WHERE YEAR (DATA_NAIX) =1960;
```
```sql
SELECT *, 
YEAR(DATA_NAIX), 
MONTH(DATA_NAIX),
monthname(DATA_NAIX),
DAY (DATA_NAIX),
dayname(DATA_NAIX)
FROM MALALT
```
```sql
SELECT * 
FROM MALALT 
WHERE DATA_NAIX LIKE '1960%' 
```
**6. Muestre a los enfermos nacidos a partir del año 1960.**
```sql
SELECT *
FROM MALALT
WHERE DATA_NAIX >= '1960-01-01'; 
```
OTRAS OPCIONES
```sql
WHERE YEAR (DATA_NAIX) >=1960;

CURRENT_DATE() //// COMANDO PARA LA FECHA DE HOY
```
## EMPRESA
**1. Muestre los productos (código y descripción) que comercializa la empresa.**
```sql
SELECT PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE;
```
Con el `AS` estamos renombrando las columnas a nuestro gusto para cuando hagamos la consulta.

**2. Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.**
 
 ```sql
SELECT PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```

**3. Muestre el código, nombre, área y teléfono de los clientes de la empresa.**
 ```sql
SELECT
CLIENT_COD AS "Código",
NOM AS "Nombre",
AREA AS "Area",
TELEFON AS "Teléfono"
FROM CLIENT;
```
**4. Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.**

 ```sql
SELECT 
CLIENT_COD AS "Código",
NOM AS "Nombre",
CIUTAT AS "Ciudad"
FROM CLIENT
WHERE AREA != 636;
```
**5. Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).**
 ```sql
SELECT 
COM_NUM AS "Código",
COM_DATA AS "Fechas de orden",
DATA_TRAMESA AS "Fecha de envío"
FROM COMANDA;
```
OTRO TIPO DE BUSQUEDAS

**-sacar una lista de todo lo que sea diferente a null en la columna de comissio**
 
 ```sql
SELECT *
FROM EMP
WHERE COMISSIO is null ;
-- WHERE COMISSIO not is null ;
-- WHERE COMISSIO >=0 ;
-- NULL no es un numero y no se puede utilizar el =
```
**-Mostrar los 4 empleados que más salario tienen**
 ```sql
SELECT *
FROM EMP
ORDER BY SALARI DESC
LIMIT 4;
```

El `ORDER BY` es para ordenar y se puede combinar con `ASC` (Ascendente), `DESC` (Descendente) y `LIMIT`(Cuantos quiere ordenar).

**-Cuales son los oficios de esta empresa.**
 ```sql
SELECT DISTINCT OFICI
FROM EMP;
-- El distinc es para que no se repitan los oficios en este caso
```
La palabra `DISTINCT` en este caso se usa para que no se repitan las palabras que buscas, en este caso si hay 30 registros de oficios, hay muchos que se repiten, pues con esta clave no se repitirían.

**-Dime cuantos oficios hay.**

 ```sql
SELECT  COUNT( DISTINCT OFICI)
FROM EMP;
-- De esta forma te cuenta cuantos tipos de oficios hay
```
El `COUNT` sirve para contar, y si lo combinamos en este caso con `DISTINCT` nos contará cuantos oficios diferentes hay en la lista.

## VIDEOCLUB

**6. Lista de nombres y teléfonos de los clientes.**

```sql
SELECT 
Nom AS "nombres",
Telefon AS "Teléfono"
FROM CLIENT;
```
**7. Lista de fechas e importes de las facturas.**

```sql
SELECT 
Data AS "Fecha",
Import AS "Importes" 
FROM FACTURA;
```
Aqui `Data` es una columna y la estamos renombrando a "Fecha".

**8.Lista de productos (descripción) facturados en la factura número 3.**

```sql
SELECT 
CodiFactura AS "Factura",
Descripcio AS "Descripción"
FROM DETALLFACTURA
WHERE CodiFactura = '3';
```

**9. Lista de facturas ordenada de forma decreciente por importe.**
```sql
SELECT *
FROM FACTURA
ORDER BY Import DESC;
-- ORDER BY Import ASC
```
**10. Lista de los actores cuyo nombre comience por X.**

```sql
SELECT*
FROM ACTOR
WHERE Nom LIKE ("X%");
```

## RETO III VIDEOCLUB

**1. Lista todas las películas del videoclub junto al nombre de su género.**
```sql
SELECT P.Titol, G.Descripcio
FROM PELICULA AS P
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
**2.Lista todas las facturas de María.**

```sql
SELECT C.*, F.CodiFactura, F.Data, F.Import
FROM CLIENT AS C
JOIN FACTURA AS F
ON C.DNI = F.DNI
WHERE C.NOM LIKE "Maria%";
```
**3.Lista las películas junto a su actor principal.**
```sql
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
```
**4.Lista películas junto a todos los actores que la interpretaron.**
```sql
SELECT P.Titol, A.Nom  
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli= I.CodiPeli
AND I.CodiActor = A.CodiActor;
```
## COMANDOS AÑADIR/INSERTAR DATOS

```sql
INSERT INTO ACTOR
VALUES (5, "Xordi"), (6, "Xavi");
```

```sql
INSERT INTO ACTOR
VALUES (5, "null");
-- el null siempre se puede poner, siempre y cuando no tenga la opcion de not null
```

```sql
INSERT INTO ACTOR
(Nom, CodiActor)
VALUES ("xoxo", 8);
-- de esta forma podemos ordenar los datos de como insertarlos
```
## COMANDOS ELIMINAR DATOS
Importante:Para eliminar datos solo se puede hacer borrando con la clave primaria. Si no, no funcionará.

```sql
-- Borrar una fila
DELETE FROM ACTOR
WHERE CodiActor=7;
```

```sql
-- Borrar varias filas
DELETE FROM ACTOR
WHERE CodiActor IN (9,10);
```

```sql
-- Borrar rango de números
DELETE FROM ACTOR
WHERE CodiActor BETWEEN 9 AND 10;
```

## COMANDOS MODIFICAR DATOS

```sql
/*Modificar un dato*/
UPDATE ACTOR
SET Nom = "Jordi"
WHERE CodiActor=5;
```
## COMO ELIMINAR UNA BASE DE DATOS

```sql
DROP DATABASE videoclub;
```







