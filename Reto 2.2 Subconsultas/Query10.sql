#Muestra las 10 canciones más compradas.Ok
USE chinook;
SELECT Track.Name AS Nombre_canción, COUNT(*) AS Compras
FROM Track
JOIN InvoiceLine
ON Track.TrackId = InvoiceLine.TrackId
GROUP BY Track.TrackId, Track.Name
ORDER BY Compras DESC
LIMIT 10;