#Muestra todas las canciones que no han sido compradas.OK
USE Chinook;
SELECT T.TrackId, T.Name 
FROM Track AS T
WHERE T.TrackId NOT IN (
	SELECT InvoiceLine.TrackId FROM InvoiceLine
);


-- LISTA DE CANCIONES QUE SI SE HAN COMPRADO
SELECT TrackId
FROM InvoiceLine;

-- DETALLES DE LAS CANCIONES QUE SI SE HAN COMPRADO
SELECT TrackId, Name , Composer
FROM Track
WHERE TrackId IN 
(	
	SELECT TrackId
	FROM InvoiceLine
);

-- CANCIONES QUE NO SE HAN COMPRADO
SELECT TrackId, Name , Composer
FROM Track
WHERE TrackId NOT IN 
(	
	SELECT TrackId
	FROM InvoiceLine
);
