#Canciones vendidas más que la media de ventas por canción.REVISAR
USE Chinook;
SELECT Name,
	(SELECT AVG(Quantity) FROM invoiceline) AS "media",
    (SELECT AVG(invoiceId) FROM invoiceline) AS "canciones_vendidas"
FROM track
WHERE "media" > "canciones_vendidas";



