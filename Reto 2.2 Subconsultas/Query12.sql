#Para demostrar lo bueno que es nuestro servicio, muestra el número de países donde tenemos clientes, 
#el número de géneros músicales de los que disponemos y el número de pistas.ok

USE chinook;
SELECT  COUNT(DISTINCT Country) AS "Número de paises",
	(SELECT COUNT(DISTINCT Name)   FROM  Genre) AS "Número de generos",
    (SELECT COUNT(DISTINCT Name) FROM Track) AS "Número de pistas"
FROM Customer;