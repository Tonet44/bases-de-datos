SELECT
	Composer,
    AVG(Milliseconds)/1000 AS "Media (s)"
FROM Track
GROUP BY Composer
ORDER BY AVG(Milliseconds);