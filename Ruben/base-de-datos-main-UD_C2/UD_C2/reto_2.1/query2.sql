-- Selecciono todo de la tabla de Invoice y con el "BETWEEN" le indico entre que fechas quiero que salgan los datos.

SELECT * FROM Chinook.Invoice
WHERE Invoice.InvoiceDate BETWEEN "2024-01-01" AND "2024-03-31";