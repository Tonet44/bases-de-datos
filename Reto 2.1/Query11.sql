#Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
USE Chinook;
SELECT C.LastName, C.FirstName, I.Total
FROM Customer AS C
JOIN Invoice AS I
ON C.CustomerId = I.InvoiceId  
WHERE I.Total >= '10'
ORDER BY C.LastName DESC;
