# Reto 2.1: Consultas básicas IV

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos  `chinook_MySql`, que nos viene dado en el fichero `Chinook_MySql.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/Reto%202.1

## Query 1
**Encuentra todos los clientes de Francia.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM CUSTOMER AS C
WHERE C.Country = "France";
```
Como no especifica nada de que columnas quiere que muestre puse el `SELECT` con asterisco para que salga toda la información del cliente.
He utilizado el `WHERE` para filtrar por país, añadiendo cual es la columna donde tiene que filtrar, en este caso `C.Country`.

## Query 2
**Muestra las facturas del primer trimestre de este año.** 

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM INVOICE AS I
WHERE MONTH (I.InvoiceDate) BETWEEN 1 AND 3
AND YEAR (I.InvoiceDate)= 2024;
```
En el `SELECT` he dejado el asterisco para que salga más información ya que el enunciado no específica nada.
He utilizado el `WHERE MONTH` para filtrar por meses en la columna `InvoiceDate`, como pide el primer trimestre he puesto `BETWEEN 1 AND 3` que incluiria los tres primeros meses del año.
Por ultimo como pide que son las facturas de este año, utilizo `AND YEAR` para filtrar el año deseado

## Query 3
**Muestra todas las canciones compuestas por AC/DC.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM Track AS T
WHERE T.Composer = "AC/DC";
```
En la tabla `Track` filtramos con el `WHERE` en la columna `Composer` y colocando entre comillas la palabra clave que buscamos `"AC/DC"`.


## Query 4
**Muestra las 10 canciones que más tamaño ocupan**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT T.Name, T.Bytes
FROM Track AS T
ORDER BY T.Bytes DESC
LIMIT 10;
```
En el `SELECT` he puesto Name y Bytes para que solo salgan estos dos datos para poder ver más claro lo que nos piden.
Para poder oredenarlo he utilizado `ORDER BY T.Bytes DESC` Bytes es la columna en la que vamos a ordenar el tamaño de las canciones y con el `DESC` le estamos indicando que lo haga de forma descendente.
Por último ponemos un `LIMIT 10` para que solo ponga los 10 primeros.


## Query 5
**Muestra el nombre de aquellos países en los que tenemos clientes.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT DISTINCT C.Country
FROM Customer AS C;
```
Para que no se repitan los países he puesto `DISTINCT` seguido de la columna que vamos a buscar, en este caso `Country`, y en el `SELECT` ponemos la tabla en la que queremos buscar la información, este caso `Customer`.

## Query 6
**Muestra todos los géneros musicales.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT DISTINCT G.Name
FROM Genre AS G;
```

Para que no se repitan los generos he puesto `DISTINCT` seguido de la columna que vamos a buscar, en este caso `Name`, y en el `SELECT` ponemos la tabla en la que queremos buscar la información, este caso `Genre`.


## Query 7
**Muestra todos los artistas junto a sus álbumes.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT A.Name, B.Title
FROM Artist AS A
JOIN Album AS B
ON A.ArtistId = B.ArtistId;
```
En primer lugar en el `SELECT` pongo las columnas de `Name` y `Title` para que solo me salgan esas dos que son las que pide el ejercicio.
Para poder sacar estos dos datos he tenido que realizar un `JOIN` con las tablas `Artist` y `Album` para poder relacionarlas entre sí junto el `ON`. 
`ArtistId` (PK), y `ArtistId` (FK).


## Query 8
**Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT E.LastName, E.FirstName, E.Title, E.BirthDate
FROM Employee
ORDER BY BirthDate DESC
LIMIT 5;
```
Yo no he visto que tengan ninguna tabla donde refleje los nombres de los supervisores. Por lo tanto lo hice de la siguiente forma.

En el `SELECT` he puesto 4 columnas para que se pueda ver mas claro lo que buscamos.
Para poder ordenar por edad, he cogido la columna donde pone la fecha de nacimiento de los empleados y lo puse así `ORDER BY BirthDate DESC` el DESC es para que lo ordene de mayor a menor.
Para finalizar se coloca el `LIMIT 5` para que solo salgan los 5 primeros.

## Query 9
**Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT I.InvoiceDate, C.FirstName, C.LastName, I.BillingAddress, I.BillingPostalCode, C.City, I.Total
FROM Invoice AS I
JOIN Customer AS C
ON I.InvoiceId = C.CustomerId
WHERE C.City = "Berlin";
```

Para poder ordenar las columnas en el orden que pone en el enunciado, solo hay que colocarlas en ese orden el en el `SELECT` 
Para poder sacar los resultados del este enunciado hay que usar dos tablas y relacionarlas con `JOIN`, relacionando con el `ON` las claves `InvoicedId` y `CustomerId` siendo las dos claves primarias.
Como en el enunciado pone que quiere los clientes que sean berlineses, he cogido de la columna `City` de la tabla `Customer` y he filtrado usando el `WHERE` y poniendo la palabra clave "Berlin".


## Query 10
**Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT P.Name AS Lista_repro, T.Name AS Nombre_Canción, A.Title AS Album, T.Milliseconds AS Duración
FROM Playlist AS P
JOIN PlaylistTrack AS PT ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T ON PT.TrackId = T.TrackId
JOIN Album AS A ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'C%'
ORDER BY Album, Duración;
```

En el `SELECT` he colocado lo que nos interesa ver que son la lista de reproducción, el nombre de la canción, el álbum y la duración.
He tenido que relacionar tres tablas con `JOIN`, la tabla `Playlist` con la de `PlaylistTrack` y esta ultima con la de `Album`.
Luego con el `WHERE` he filtrado en la columna de `Name` de la tabla `Playlist` para que me salgan solo los que empiecen con la letra "c", de la siguiente forma `C%`.
Luego los he ordenado con el `ORDER BY` como dice el enunciado con el álbum y la duración.

## Query 11
**Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT C.LastName, C.FirstName, I.Total
FROM Customer AS C
JOIN Invoice AS I
ON C.CustomerId = I.InvoiceId  
WHERE I.Total >= '10'
ORDER BY C.LastName DESC;
```

Para que se muestre mas claro lo que nos piden, aunque no lo especifique he colocado en el `SELECT` tres columnas solo, el nombre, apellido y el dinero.

Para poder sacar los resultados del este enunciado hay que usar dos tablas y relacionarlas con `JOIN`, relacionando con el `ON` las claves `InvoicedId` y `CustomerId` siendo las dos claves primarias.
Luego he usado `WHERE I.Total >= '10'` para poder filtrar y que me salgan solo los importes mayor o igual a 10.
Por último para ordenarlo de mayor a menor he usado `ORDER BY C.LastName DESC`


## Query 12
**Muestra el importe medio, mínimo y máximo de cada factura.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT I.InvoiceId AS Id_factura,
AVG(I.Total) AS Importe_Medio,
MIN(I.Total) AS Importe_Minimo,
MAX(I.Total) AS Importe_Maximo
FROM Invoice AS I
GROUP BY Id_factura;
```
En el `SELECT` colocamos sobre que vamos a calcular los importes en este caso las facturas y lo agruparemos con el `GROUP BY` con el id_factura.
Para calcular el importe medio, mínimo y máximo se hace con las funciones de agregación `AVG`, `MIN` y `MAX` respectivamente, poniendo entre parentesis con que columna quieres realizar la operación.


## Query 13
**Muestra el número total de artistas**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT COUNT(A.Name) AS Numero_de_Artistas
FROM Artist AS A;
```

Para poder hacer esta Query hay que contar el número de artistas que hay, yo lo hice con el `COUNT` poniendo entre parentesis que columna es la que quiero contar, en este caso sería la de `Name`, y lo renombro con el `AS` para que salga más claro.

## Query 14
**Muestra el número de canciones del álbum “Out Of Time”**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT A.Title, COUNT(*) AS Canciones
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
WHERE A.Title = "Out Of Time";
```

Para poder hacer este ejercicio hay que contar el número de canciones de un album, empezamos contando con el `COUNT` y poniendo título a nuestra nueva columna que se llamará Canciones y es donde aparecera nuestro sumatorio.
He tenido que relacionar dos tablas, la de `Album` y la de `Track`, con `Album.AlbumId` (PK) y `Track.AlbumId` (FK).
Por último filtre con la columna de `Title` de la tabla `Album` para que me cuente solo del Album "Out Of Time".

## Query 15
**Muestra el número de países donde tenemos clientes.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT COUNT(DISTINCT C.Country) AS Num_países
FROM Customer AS C;
```

Para poder contar el número de países donde hay clientes lo hice con el `COUNT(DISTINCT C.Country)`.
Puse un `DISTINCT` para que no se repitieran los países, luego lo renombre con el `AS` para que saliera mas bonito.

## Query 16
**Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT DISTINCT G.Name AS Género, COUNT(DISTINCT T.Name) AS Numero_Canciones 
FROM Genre AS G
JOIN track AS T
ON G.GenreId = T.GenreId
GROUP BY G.Name;
```

En esta Query use dentro del `SELECT` dos veces `DISTINCT` para que no se repitieran ni el género ni el número de canciones.
El `COUNT` lo hice con la columna de `T.Name` que es la que voy a contar.
tuve que relacionar dos tablas para poder hacer el ejercicio, `Genre` y `Track` con `G.GenreId` (PK) `T.GenreId` (FK).
Luego lo agrupe todo con el nombre del género.

## Query 17
**Muestra los álbumes ordenados por el número de canciones que tiene cada uno.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT DISTINCT A.Title, COUNT(T.AlbumId) AS Numero_Canciones
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY Numero_Canciones DESC;
```
En este caso para contar el número de canciones lo hice usasndo el `COUNT` con la columna `AlbumId`, además añadí el título del album colocandole un `DISTINCT` para que no se repita.
tuve que relacionar dos tablas para poder hacer el ejercicio, `Album` y `Track` con `A.AlbumId` (PK) `T.AlbumId` (FK).
Luego lo agrupe todo con el número de canciones y de forma descendente.

## Query 18
**Encuentra los géneros musicales más populares (los más comprados).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT G.Name, COUNT(T.UnitPrice) AS Total
FROM Genre AS G
JOIN Track AS T
ON G.GenreId = T.GenreId
GROUP BY G.Name
ORDER BY Total DESC
LIMIT 5;
```

En este ejercicio decidí contarlo con la columna del dinero `UnitPrice` con un `COUNT` y renombrarlo como total, además puse también la columna del nombre del género. Todo esto en el `SELECT`
Tuve que relacionar dos tablas para poder hacer el ejercicio, `Genre` y `Track` con `G.GenreId` (PK) `T.GenreId` (FK).
Lo agrupe todo con el nombre del género con `GROUP BY`, lo ordene de forma descendente con el `ORDER BY` y puse un `LIMIT 5` para hacer un top five.

## Query 19
**Lista los 6 álbumes que acumulan más compras.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT A.Title, COUNT(T.AlbumId) AS Compras
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY Compras DESC
LIMIT 6;
```
Para contar los álbumes que más compras tienen he usado un `COUNT` con la columna AlbumId, para que además me salieran los nombres. También puse el título del album `A.Title`. Todo ello dentro del `SELECT`.
Tuve que relacionar dos tablas para poder hacer el ejercicio, `Album` y `Track` con `A.AlbumId` (PK) `T.AlbumId` (FK).
Lo agrupe todo con el título del album con `GROUP BY`, lo oredene de forma descendente con el `ORDER BY` y puse un `LIMIT 6` para que solo aparezcan 6 registros.


## Query 20
**Muestra los países en los que tenemos al menos 5 clientes**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT C.Country, COUNT(C.FirstName) Total_Clientes
FROM Customer AS C
GROUP BY C.Country
HAVING COUNT(C.FirstName) >= 5
ORDER BY Total_Clientes DESC;
```

En el `SELECT` puse la columna de Country sin el DISTINCT porque no se repiten los países y no era necesario, luego el `COUNT` contando con el primer nombre del cliente para saber cuantos clientes tenemos por país.
Todo esto esta en la tabla de Customers y para ello lo coloco en el `FROM`.
Tuve que hacer un `HAVING` para poder filtrar el `COUNT` para que se de almenos 5 clientes con ">=5".
Luego lo ordene de forma descendente y por el número total de cliente con el `ORDER BY`.