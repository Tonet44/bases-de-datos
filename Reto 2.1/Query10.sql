#Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
#sus canciones, ordenadas por álbum y por duración.
USE Chinook;
SELECT P.Name AS Lista_repro, T.Name AS Nombre_Canción, A.Title AS Album, T.Milliseconds AS Duración
FROM Playlist AS P
JOIN PlaylistTrack AS PT ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T ON PT.TrackId = T.TrackId
JOIN Album AS A ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'C%'
ORDER BY Album, Duración;