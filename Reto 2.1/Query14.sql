#Muestra el número de canciones del álbum “Out Of Time”.
USE Chinook;
SELECT A.Title, COUNT(*) AS Canciones
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
WHERE A.Title = "Out Of Time";