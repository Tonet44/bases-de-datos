#Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas:
#fecha de la factura, nombre completo del cliente, dirección de facturación,
#código postal, país, importe (en este orden).
USE Chinook;
SELECT I.InvoiceDate, C.FirstName, C.LastName, I.BillingAddress, I.BillingPostalCode, C.City, I.Total
FROM Invoice AS I
JOIN Customer AS C
ON I.InvoiceId = C.CustomerId
WHERE C.City = "Berlin";