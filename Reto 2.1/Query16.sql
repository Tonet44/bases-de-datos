#Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
USE Chinook;
SELECT DISTINCT G.Name AS Género, COUNT(DISTINCT T.Name) AS Numero_Canciones 
FROM Genre AS G
JOIN Track AS T
ON G.GenreId = T.GenreId
GROUP BY G.Name;

SELECT Genre.Name, COUNT(*)
FROM Track
JOIN Genre



