#Encuentra los géneros musicales más populares (los más comprados).
USE Chinook;
SELECT G.Name, COUNT(*) AS Total
FROM Genre AS G
JOIN Track AS T
ON G.GenreId = T.GenreId
GROUP BY G.Name
ORDER BY Total DESC
LIMIT 5;