#Muestra los países en los que tenemos al menos 5 clientes.
SELECT c.Country, COUNT(C.FirstName) Total_Clientes
FROM Customer AS C
GROUP BY C.Country
HAVING COUNT(C.FirstName) >= 5
ORDER BY Total_Clientes DESC;