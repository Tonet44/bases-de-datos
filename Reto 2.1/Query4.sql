#Muestra las 10 canciones que más tamaño ocupan.
USE Chinook;
SELECT T.Name, T.Bytes
FROM Track AS T
ORDER BY T.Bytes DESC
LIMIT 10;