#Muestra todos los artistas junto a sus álbumes.
USE Chinook;
SELECT A.Name, B.Title
FROM Artist AS A
JOIN Album AS B
ON A.ArtistId = B.ArtistId;