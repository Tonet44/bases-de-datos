#Lista los 6 álbumes que acumulan más compras.
USE Chinook;
SELECT A.Title, COUNT(T.AlbumId) AS Compras
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY Compras DESC
LIMIT 6;