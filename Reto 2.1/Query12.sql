#Muestra el importe medio, mínimo y máximo de cada factura.
USE Chinook;
SELECT I.InvoiceId AS Id_factura,
AVG(I.Total) AS Importe_Medio,
MIN(I.Total) AS Importe_Minimo,
MAX(I.Total) AS Importe_Maximo
FROM Invoice AS I
GROUP BY Id_factura;