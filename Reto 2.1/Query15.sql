#Muestra el número de países donde tenemos clientes.
USE Chinook;
SELECT COUNT(DISTINCT C.Country) AS Num_Paises
FROM Customer AS C;