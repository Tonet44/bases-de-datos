#Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.
USE Chinook;
SELECT E.LastName, E.FirstName, E.Title, E.BirthDate
FROM Employee AS E
ORDER BY BirthDate DESC
LIMIT 5;


SELECT 
E1.EmployeeId AS "Id Empleado", 
CONCAT(E1.LastName, "," , E1.FirstName) "Empleado",
E2.EmployeeId AS "Id Supervisor",
CONTACT(E2.LastName, ",", E2.FirstName) "Supervisor",
E2.FirstName 
FROM Employee AS E1
JOIN Employee AS E2
ON E1.ReportsTo = E2.EmployeeId
ORDER BY E1.BirthDate DESC
LIMIT 5;