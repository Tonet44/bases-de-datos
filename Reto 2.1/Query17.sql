#Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
USE Chinook;
SELECT DISTINCT A.Title, COUNT(T.AlbumId) AS Numero_Canciones
FROM Album AS A
JOIN Track AS T
ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
ORDER BY Numero_Canciones DESC;