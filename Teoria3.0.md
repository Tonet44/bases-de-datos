Antonio Esteban Lorenzo / 1DAW Bases de datos


# MEMORIA 3.0

## SUBCONSULTAS
Una subconsulta es una consulta anidada dentro de otra consulta.

### TIPOS DE SUBCONSULTAS
El estándar SQL define tres tipos de subconsultas:

- **Subconsultas de fila:** Son aquellas que devuelven más de una columna pero una única fila.

- **Subconsultas de tabla.** Son aquellas que devuelve una o varias columnas y cero o varias filas.

- **Subconsultas escalares.** Son aquellas que devuelven una columna y una fila.

Las subconsultas se pueden utilizar en varias partes de una consulta SQL, como:

- **En la cláusula WHERE:** para filtrar los datos que se devuelven en la consulta principal.

- **En la cláusula FROM:** para obtener datos adicionales que se necesitan para la consulta principal.

- **En la cláusula SELECT:** para realizar cálculos que se necesitan para la consulta principal.

### OPERADORES QUE PODEMOS UTILIZAR EN LAS SUBCONSULTAS

Los operadores que podemos usar en las subconsultas son los siguientes:

**Operadores básicos de comparación (>,>=, <, <=, !=, <>, =):** 

Se pueden usar cuando queremos comparar una expresión con el valor que devuelve una subconsulta.

Los operadores básicos de comparación los vamos a utilizar para realizar comparaciones con subconsultas que devuelven un único valor, es decir, una columna y una fila.

**Predicados ALL y ANY:**

Se utilizan con los operadores de comparación (>,>=, <, <=, !=, <>, =) y nos permiten comparar una expresión con el conjunto de valores que devuelve una subconsulta.

ALL y ANY los vamos a utilizar para realizar comparaciones con subconsultas que pueden devolver varios valores, es decir, una columna y varias filas.

**Predicado IN y NOT IN**

Nos permiten comprobar si un valor está o no incluido en un conjunto de valores, que puede ser el conjunto de valores que devuelve una subconsulta.

IN y NOT IN los vamos a utilizar para realizar comparaciones con subconsultas que pueden devolver varios valores, es decir, una columna y varias filas.

**Predicado EXISTS y NOT EXISTS**

## CAMPOS GENERADOS



## QUERYS EXAMEN

```sql
-- Duración media, en segundos y redondeando a las unidades, de la segunda playlist con más canciones

SELECT *
FROM playlisttrack;

SELECT *
FROM Track;

SELECT PlaylistId, Count(TrackId) as total_canciones
FROM playlisttrack
GROUP BY PlaylistId
ORDER BY total_canciones DESC;

SELECT *
FROM Track
WHERE TrackId in (SELECT TrackId FROM playlisttrack WHERE PlaylistId = 5);


-- RESULTADO FINAL
SELECT  ROUND(AVG(Milliseconds) / 1000, 0) AS total
FROM Track
WHERE TrackId in (SELECT TrackId FROM playlisttrack WHERE PlaylistId = 5);
```


