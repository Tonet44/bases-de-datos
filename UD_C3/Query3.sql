/*Listar las peliculas con su actor principal*/
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
