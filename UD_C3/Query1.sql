/*Nombre película y nombre género*/
SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA, GENERE;

SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA
JOIN GENERE
ON PELICULA.CodiGenere = GENERE.CodiGenere; 

SELECT P.Titol, G.Descripcio
FROM PELICULA AS P
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
