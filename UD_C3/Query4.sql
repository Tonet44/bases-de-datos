/*Peliculas mas todos sus actores*/
SELECT P.Titol, A.Nom  
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli= I.CodiPeli
AND I.CodiActor = A.CodiActor;