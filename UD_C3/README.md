# Reto 3: Consultas básicas III

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos  `videoclub`, que nos viene dado en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/UD_C3

## Query 1
**Lista todas las películas del videoclub junto al nombre de su género.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT P.Titol, G.Descripcio
FROM PELICULA AS P
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
Para poder trabajar mejor hemos dado alias a las tablas `PELICULA` y `GENERE` lo hemos hecho con el `AS` y ahora son `P.` y `G.` respectivamente.
En el `P.Titol` y `G.Descripcio`, son las dos columnas que vamos a seleccionar para ver la información de las tablas `PELICULA` y `GENERE`.
La tabla `PELICULA` la unimos con `GENERE` con el `JOIN` y le diremos con el `ON` Cuales son las dos claves que coinciden de las dos tablas, en este caso serían `CodiGenere` de la tabla `PELICULA` y `Descripcio` de la tabla `GENERE` donde la clave principal o primaria sería el `G.CodiGenere` y la foranea sería `P.CodiGenere`.

## Query 2
**Lista todas las facturas de María.** 

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT C.*, F.CodiFactura, F.Data, F.Import
FROM CLIENT AS C
JOIN FACTURA AS F
ON C.DNI = F.DNI
WHERE C.NOM LIKE "Maria%";
```
Renombramos con un alias las tablas `CLIENT` y `FACTURA` con el `AS` como `C.` y `F.` respectivamente.
En el `SELECT` ponemos las columnas que vamos a seleccionar para ver por pantalla, de `CLIENT` con el "*" seleccionamos todas las columnas, y de `FACTURA` seleccionaremos `CodiFactura`, `Data` y `Import`.
En el `FROM` seleccionamos la tabla `CLIENT` y en el `JOIN` la tabla `FACTURA` que son las tablas que vamos a juntar. En el `ON` tenemos que poner las claves principal y foranea de las tablas a relacionar. En este caso la clave principal o primaria sería `CLIENT.DNI` y la clave foranea sería `FACTURA.DNI`.
Para finalizar nos pide que sean las facturas de Maria, para ello lo hacemos de la siguiente forma `WHERE C.NOM LIKE "Maria%"` en la columna `NOM` de la tabla `CLIENT` filtramos con el `LIKE` y buscamos la palabra Maria, le ponemos el "%" para que pueda coger cualquier letra desppues de Maria.

## Query 3
**Lista las películas junto a su actor principal.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
```
Renombramos con un alias las tablas `PELICULA` y `ACTOR` con el `AS` como `P.` y `A.` respectivamente.
En el `SELECT` ponemos las columnas que vamos a seleccionar para ver por pantalla, de `PELICULA`  seleccionamos `P.Titol`, y de `ACTOR` seleccionaremos `A.Nom`.
En el `FROM` seleccionamos la tabla `PELICULA` y en el `JOIN` la tabla `ACTOR` que son las tablas que vamos a juntar. En el `ON` tenemos que poner las claves principal y foranea de las tablas a relacionar. En este caso las dos son clave principal o primaria serían `CLIENT.DNI` y `FACTURA.DNI`.


## Query 4
**Lista películas junto a todos los actores que la interpretaron.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT P.Titol, A.Nom  
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli= I.CodiPeli
AND I.CodiActor = A.CodiActor;
```
Renombramos con un alias las tablas `PELICULA`, `ACTOR` y `INTERPRETADA` con el `AS` como `P.`, `A.` y `I.` respectivamente.
En el `SELECT` ponemos las columnas que vamos a seleccionar para ver por pantalla, de `PELICULA`  seleccionamos `P.Titol`, y de `ACTOR` seleccionaremos `A.Nom`.
En el `FROM` seleccionamos la tabla `PELICULA`, en el primer `JOIN` la tabla `INTERPRETADA` y en el segundo `ACTOR`que son las tablas que vamos a juntar.
En el `ON` tenemos que poner las claves principal y foranea de las tablas a relacionar como en este caso tenemos tres tablas añadiremos el `AND`. En primer lugar uniremos `P.CodiPeli` y `I.CodiPeli` estas dos son principales o primarias.
En segundo lugar uniremos `I.CodiActor` y `A.CodiActor` estas dos tambien son primarias o principales.


## Query 5
**Lista ID y nombres de las películas junto a los ID y nombres de sus segundas
partes.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT P.CodiPeli, P.Titol, P.SegonaPart, I.CodiPeli
FROM PELICULA AS P
JOIN INTERPRETADA AS I;
```
En esta query he unido las dos tablas de `PELICULA` y `INTERPRETADA` para que me salgan todas sus peliculas con los codigos de las dos partes. He usado el `SELECT` para seleccionar las columnas que quiero que salgan. He usado el `JOIN` para unir las dos tablas. 


