#Administradores que han dado más de  2 “Me gusta”.
USE instagram_low_cost;
SELECT U.* , COUNT(RC.idTipoReaccion) AS Num_likes
FROM usuarios AS U
JOIN reaccionesComentarios AS RC
ON U.idUsuario = RC.idUsuario
WHERE U.idRol= 3 AND RC.idTipoReaccion= 1
GROUP BY U.idUsuario
HAVING Num_likes > 2;

