#Fotos del usuario con ID 36 tomadas en enero del 2023
USE instagram_low_cost;
SELECT *
FROM fotos
WHERE idUsuario = 36
AND MONTH (fechaCreacion) = 1
AND YEAR (fechaCreacion)= 2023;