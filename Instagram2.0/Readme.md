# Reto: INSTAGRAM2.0

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos  `instagram_low_cost`, que nos viene dado en el fichero `intagram.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos.git

## Query 1
**Fotos del usuario con ID 36**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT *
FROM fotos
WHERE idUsuario = 36;
```
Seleccionamos la tabla de `fotos` y lo filtramos con el `WHERE` de la columna `idUsuario=36` así podremos obtener las fotos del usuario 36.

## Query 2
**Fotos del usuario con ID 36 tomadas en enero del 2023** 

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT *
FROM fotos
WHERE idUsuario = 36
AND MONTH (fechaCreacion) = 1
AND YEAR (fechaCreacion)= 2023;
```
En el `SELECT *` seleccionamos toda la información de la tabla fotos. Con el `WHERE` filtramos por el `id usuario` para obtener al usuario 36, ademas filtramos por el mes 1 y el año 2023 con el `MONTH` y el `YEAR` respectivamente.


## Query 3
**Comentarios del usuario 36 sobre la foto 12 del usuario 11**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT *
FROM fotos AS F
JOIN comentariosFotos AS CF
ON F.idUsuario = CF.idFoto
JOIN comentarios AS C
ON CF.idComentario = C.idComentario
WHERE F.idUsuario= 11 AND C.idUsuario = 36 AND F.idFoto = 12;
```
Para realizar esta consulta he necesitado tres tablas y las he unido con `JOIN`.
La tabal fotos la he unido con la de comentariosFotos con `idUsuario` y `idFoto` respectivamenete.
La tabal comentariosFotos la he unido con la de comentarios con `idComentario` y `idComentario` respectivamenete.
Para finalizar he filtrado con el `WHERE` escogiendo el `idUsuario` 11 de la tabla fotos, el `idUsuario` 36 de la tabla comentarios y el `idFoto` 12 de la tabla fotos.

## Query 4
**Fotos que han sorprendido al usuario 25**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT *
FROM fotos AS F
JOIN reaccionesFotos AS R
ON F.idFoto = R.idFoto
WHERE R.idUsuario= 25;
```
Para realizar esta consulta he unido dos tablas con un `JOIN`, la tabla `fotos` y la tabla `reaccionesFotos` y la he unido con `idFoto` de la tabal fotos y `idFoto` de la tabla reaccionesFotos.
Por ultimo filtramos con el `WHERE` para buscar el usuario 25.

## Query 5
**Administradores que han dado más de  2 “Me gusta”.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT U.* , COUNT(RC.idTipoReaccion) AS Num_likes
FROM usuarios AS U
JOIN reaccionesComentarios AS RC
ON U.idUsuario = RC.idUsuario
WHERE U.idRol= 3 AND RC.idTipoReaccion= 1
GROUP BY U.idUsuario
HAVING Num_likes > 2;
```

En esta consulta he realizado dos consultas antes de empezar, una para saber el id del administrador que es 3 y otra para saber el id del "me gusta" que es 1.
Sabiendo esto he utilizado dos tablas, la tabla usuarios y la tabla reaccionesComentarios uniendolas con un `JOIN`, y las he relacionado entre ellas con el idRol de la tabla usuarios y con el idTipoReaccion de la tabla reaccionesComentarios.
He filtrado con el `WHERE` utilizando los datos que obtuvimos en las dos consultas externas que hicimos anteriormente para filtrar por administrador y me gusta.
Por ultimo para contar los me gusta he utilizado un `COUNT` agrupandolo por el idUsuario de la tabla usuarios, y filtrando con un `HAVING` para que me salgan los que tienen más de 2 me gusta.

## Query 6
**Número de “Me divierte” de la foto número 12 del usuario 45**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT COUNT(*) AS Total
FROM reaccionesFotos  AS R
WHERE R.idFoto= 12 AND R.idUsuario= 45 AND R.idTipoReaccion= 3;
```
Para esta consulta he utilizado un `COUNT` en la tabla reaccionesFotos y lo he filtrado con el idFoto 12, el idUsuario 45 y el idTipoReaccion 3.

## Query 7
**Número de fotos tomadas en la playa (en base al título)**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE instagram_low_cost;
SELECT COUNT(*)
FROM fotos AS F
WHERE F.descripcion LIKE '%playa%';
```
En esta consulta he utilizado la tabla de fotos.
Para poder contar el número de veces que aparece la palabra playa en la descripción de los comentarios he utilizado un `COUNT` y lo he filtrado con un `WHERE` y un `LIKE` y con la clave `%playa%` para que busque esta palabra excatamente.

