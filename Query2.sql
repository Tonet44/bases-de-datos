
-- Canciones por álbum
SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId;

-- Media de canciones por álbum
SELECT AVG(n_tracks)
FROM (
	SELECT AlbumId, COUNT(*) AS n_tracks
	FROM Track
	GROUP BY AlbumId
) AS CONTEODEPISTAS;

-- Álbumes con más canciones que esta media (hacer la comparación)

SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId
HAVING n_tracks > 

(
SELECT AVG(Num_canciones)
FROM (
	SELECT T.AlbumId, COUNT(T.TrackId) AS Num_canciones
    FROM Track T
    GROUP BY T.AlbumId
    ) Ntrack
);