# Reto 2: Consultas básicas II

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos `empresa` y `videoclub`, que nos vienen dadas en los ficheros `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/UD_C2/reto_2

## Query 1
**Muestre los productos (código y descripción) que comercializa la empresa.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE;
```
En el `SELECT` ponemos las columnas que vamos a utilizar, en este caso son `PROD_NUM` y `DESCRIPCIO`, con el `AS` estamos renombrando estas dos columnas a nuestro gusto.
En `FROM` ponemos la tabla en la que queremos buscar la información.

## Query 2
**Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.** 

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```
En el `SELECT` ponemos las columnas que vamos a utilizar, en este caso son `PROD_NUM` y `DESCRIPCIO`, con el `AS` estamos renombrando estas dos columnas a nuestro gusto.
En `FROM` ponemos la tabla en la que queremos buscar la información.
Con el `WHERE` y el `LIKE` vamos a buscar la palabra "TENNIS", `DESCRIPCIO` será la columna que vamos a seleccionar y para buscar la palabra clave utilizaremos "%" `"%TENNIS%"`.

## Query 3
**Muestre el código, nombre, área y teléfono de los clientes de la empresa.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
CLIENT_COD AS "Código",
NOM AS "Nombre",
AREA AS "Area",
TELEFON AS "Teléfono"
FROM CLIENT;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `CLIENT_COD`, `NOM`, `AREA` y `TELEFON` renombradas como Código, Nombre, Area y teléfono respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `CLIENT`, que es donde están los datos que estamos buscando.

## Query 4
**Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
CLIENT_COD AS "Código",
NOM AS "Nombre",
CIUTAT AS "Ciudad"
FROM CLIENT
WHERE AREA != 636;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `CLIENT_COD`, `NOM` y `CIUTAT` renombradas como Código, Nombre y Ciudad respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `CLIENT`, que es donde están los datos que estamos buscando.
En el `WHERE` decimos que en la columna de `AREA` queremos buscar todos los números que no sean el 636 y lo hacemos con `!= 636`.

## Query 5
**Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
COM_NUM AS "Código",
COM_DATA AS "Fechas de orden",
DATA_TRAMESA AS "Fecha de envío"
FROM COMANDA;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `COM_NUM`, `COM_DATA` y `DATA_TRAMESA` renombradas como Código, Fechas de orden y Fechas de envío respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `COMANDA`, que es donde están los datos que estamos buscando.

## Query 6
**Lista de nombres y teléfonos de los clientes.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
Nom AS "nombres",
Telefon AS "Teléfono"
FROM CLIENT;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `Nom` y `Telefon` renombradas como nombres y Teléfono respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `CLIENT`, que es donde están los datos que estamos buscando.

## Query 7
**Lista de fechas e importes de las facturas.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
Data AS "Fecha",
Import AS "Importes" 
FROM FACTURA;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `Data` e `Import` renombradas como Fecha e Importes respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `FACTURA`, que es donde están los datos que estamos buscando.

## Query 8
**Lista de productos (descripción) facturados en la factura número 3.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
CodiFactura AS "Factura",
Descripcio AS "Descripción"
FROM DETALLFACTURA
WHERE CodiFactura = '3';
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son `CodiFactura` e `Descripcio` renombradas como Factura y Descripción respectivamente con el `AS`.
Con el `FROM` seleccionamos la tabla, en este caso la de `DETALLAFACTURA`, que es donde están los datos que estamos buscando.
En el `WHERE` ponemos `CodiFactura` que es la columna donde vamos a filtrar la busqueda especifica para buscar el "3" de la siguiente manera `='3'`, al poner el número sin más solo buscará ese número.

## Query 9
**Lista de facturas ordenada de forma decreciente por importe.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT *
FROM FACTURA
ORDER BY Import DESC;
-- ORDER BY Import ASC
```
En el SELECT vamos a poner un `*` para buscar todas las columnas de la tabla `FACTURA` que la seleccionaremos con el `FROM`.
Luego usaremos el `ORDER BY` para orednar los datos, si ponemos `DESC` será de forma descendente y si ponemos `ASC` será de forma ascendente.

## Query 10
**Lista de los actores cuyo nombre comience por X.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT*
FROM ACTOR
WHERE Nom LIKE ("X%");
```
En el SELECT vamos a poner un `*` para buscar todas las columnas de la tabla `ACTOR` que la seleccionaremos con el `FROM`.
Luego usaremos el `WHERE Nom LIKE ("X%")` para poder coger de la columna `Nom` y filtrar las palabras que empiecen por "X", delnte de la X no se pone nada y despues de ella se pone el signo "%" para que busque cualquier letra.