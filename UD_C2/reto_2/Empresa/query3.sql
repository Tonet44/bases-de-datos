/*Muestre el código, nombre, área y teléfono de los clientes de la empresa.*/
SELECT 
CLIENT_COD AS "Código",
NOM AS "Nombre",
AREA AS "Area",
TELEFON AS "Teléfono"
FROM CLIENT;
