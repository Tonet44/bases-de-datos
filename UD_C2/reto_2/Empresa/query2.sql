/*Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción. */
SELECT PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";