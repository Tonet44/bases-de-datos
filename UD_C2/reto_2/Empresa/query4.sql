/*Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.*/
SELECT 
CLIENT_COD AS "Código",
NOM AS "Nombre",
CIUTAT AS "Ciudad"
FROM CLIENT
WHERE AREA != 636;

