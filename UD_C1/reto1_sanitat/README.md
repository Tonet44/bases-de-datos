# Reto 1: Consultas básicas

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`.
Hemos utilizado `AS` para poder dar un nombre a la columna del código.
Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
HOSPITAL_COD AS "Código",
NOM,
TELEFON 
FROM HOSPITAL;
```

## Query 2
Para que muestre los hospitales que contengan la letra A en la segunada posición de la palabra, y además que nos diga el teléfono y el número se realiza de la siguiente manera:
```sql
SELECT *
FROM HOSPITAL
WHERE NOM LIKE '_A%';
```
En el `SELECT` ponemos el * para que nos salgan todos los datos de la tabla que vamos a buscar, en este caso la tabla que vamos a utilizar es la de `HOSPITAL`, y la seleccionaremos con el `FROM`.
Para poder ver el nombre de los hospitales cuyo nombre contenga una A en su segunda posición, usaremos el `WHERE` para seleccionar la columna que vamos a filtrar y con el `LIKE _A%` le decimos que letra y en que posición buscar. Si ubiese sido en vez de la segunada letra, la tercera sería así. LIKE __A% con dos barras bajas.

## Query 3
Para que muestre todos los trabajadores con su código de hospital, código sala, número empleado y apellido debemos de hacer lo siguiente:
```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son código de hospital `HOSPITAL_COD`, código sala `SALA_COD`, número empleado `EMPLEAT_NO` y apellido `COGNOM`.
Con el `FROM` seleccionamos la tabla, en este caso la de `PLANTILLA`, que es donde están los datos que estamos buscando.

## Query 4
Para que muestre todos los trabajadores con su código de hospital, código sala, número empleado, apellido y ademas no muestre los trabajadores con el turno de noche, debemos de hacer lo siguiente:

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM, TORN
FROM PLANTILLA
WHERE TORN IN ('M','T');
```
En el SELECT vamos a poner las columnas que queremos que salgan, en este caso son código de hospital `HOSPITAL_COD`, código sala `SALA_COD`, número empleado `EMPLEAT_NO`, apellido `COGNOM` y el turno `TORN`.
Con el `FROM` seleccionamos la tabla, en este caso la de `PLANTILLA`, que es donde están los datos que estamos buscando.
En el `WHERE` decimos que en la columna de Torn queremos filtrar con la "M" y la "T" para que salgan solo los turnos de mañana y de tarde.
Se podia haber puesto en vez de `WHERE TORN IN ('M','T')` filtrando para que salgan todos los que no sean "N" con `WHERE TORN <> 'N'`.

## Query 5
Para mostrar a los enfermos nacidos en 1960, debemos hacer lo siguiente:

```sql
SELECT *
FROM MALALT
WHERE DATA_NAIX >= '1960-01-01' AND DATA_NAIX <='1960-12-31';
```
Con el `SELECT*` podemos seleccionar todas las columnas de la tabla `MALALT` que la pondremos en el `FROM`.
En el `WHERE` seleccionamos la columna `DATA_NAIX` para poder filtar la fecha, nos pide que sean nacidos en el año 1960 y lo hemos puesto de la siguiente forma `DATA_NAIX >= '1960-01-01' AND DATA_NAIX <='1960-12-31'`.

## Query 6
Para mostrar los enfermos nacidos a partir del año 1960, debemos hacer lo siguiente:

```sql
SELECT *
FROM MALALT
WHERE DATA_NAIX >= '1960-01-01';
```
Con el `SELECT*` podemos seleccionar todas las columnas de la tabla `MALALT` que la pondremos en el `FROM`.
Con el `WHERE` filtramos en la columna `DATA_NAIX` los enfermos nacidos a partir del año 1960.







