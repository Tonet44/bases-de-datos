#Query2-Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.

SELECT a.Name
FROM artist as a;

SELECT *
FROM TRACK
WHERE Milliseconds > 50000;

SELECT a.Name FROM(
SELECT * FROM TRACK as t WHERE t.artistId = a.artistId Milliseconds > 50000);


USE Chinook;
SELECT DISTINCT A.artistId, a.Name
FROM Artist AS A
JOIN Album AS A2
ON A.ArtistId = A2.ArtistId
JOIN Track AS T
ON A2.AlbumId = T.AlbumId
WHERE T.Milliseconds > 300000
ORDER BY T.Milliseconds DESC;


