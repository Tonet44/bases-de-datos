#Query1-Muestra las listas de reproducción cuyo nombre comienza por M, junto a las 3
#primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero).

SELECT *
FROM playlist
WHERE Name LIKE "M%";

SELECT  t.Name
FROM track AS T JOIN playlisttrack as pl on t.trackId = pl.trackId
WHERE (
	SELECT *
	FROM playlist as p
	WHERE p.PlaylistId = pl.PlaylistId AND p.Name LIKE "M%")
    ;
    
    
    
    
    
    USE Chinook;
SELECT *
FROM Playlist AS P
JOIN PlaylistTrack AS P1
ON P.PlaylistId = P1.PlaylistId
JOIN Track AS T
ON P1.TrackId = T.TrackId
JOIN Album AS A
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'M%'
ORDER BY A.Title, T.UnitPrice DESC;