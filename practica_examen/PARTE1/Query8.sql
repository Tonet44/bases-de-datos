#Query8-Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.

SELECT CONCAT(FirstName, " " ,LastName) AS Empleado, ReportsTo
FROM Employee JOIN Employee;


SELECT 
    CONCAT(e1.FirstName, " ", e1.LastName) AS Empleado,
    e1.BirthDate,
    CONCAT(e2.FirstName, " ", e2.LastName) AS Supervisor
FROM 
    Employee e1
LEFT JOIN 
    Employee e2 ON e1.ReportsTo = e2.EmployeeId
ORDER BY 
    e1.BirthDate DESC
LIMIT 15;