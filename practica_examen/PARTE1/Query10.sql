#Query10-Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.

SELECT P1.Name AS Lista_repro, T.Name AS Nombre_Canción, T.Milliseconds AS Duración
FROM Playlist AS P1 JOIN playlisttrack AS P2 ON P1.PlaylistId = P2.PlaylistId
JOIN track AS T ON P2.TrackId = T.TrackId
where P1.Name LIKE 'C%'
ORDER BY T.AlbumId, T.Milliseconds;

SELECT P.Name AS Lista_repro, T.Name AS Nombre_Canción, T.Milliseconds AS Duración
FROM Playlist AS P JOIN PlaylistTrack AS PT ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T ON PT.TrackId = T.TrackId
WHERE 
    P.PlaylistId IN (
        SELECT PlaylistId
        FROM Playlist
        WHERE Name LIKE 'C%'
    )
ORDER BY T.AlbumId, T.Milliseconds;