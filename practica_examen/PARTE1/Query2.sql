#Query2-Muestra las facturas del primer trimestre de este año.

USE Chinook;
SELECT *
FROM INVOICE AS I
WHERE MONTH (I.InvoiceDate) BETWEEN 1 AND 3
AND YEAR (I.InvoiceDate)= 2024;