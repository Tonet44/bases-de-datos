#Query14-Muestra el número de canciones del álbum “Out Of Time”.

SELECT *
FROM Album
WHERE Title = "Out Of Time";

SELECT count(AlbumId) AS Num_canciones
FROM track
WHERE AlbumId=187;


SELECT COUNT(*) AS Num_canciones
FROM Track as t
WHERE AlbumId = (
        SELECT AlbumId
        FROM Album
        WHERE Title = 'Out Of Time'
    );
    
    
SELECT a.Title,
	(SELECT COUNT(*) AS Num_canciones from Track as t ) as total
FROM Album as a
WHERE (SELECT AlbumId
        FROM Album
        WHERE Title = 'Out Of Time');
        
-- ESTA ES LA BUENA
SELECT a.Title,
       (SELECT COUNT(*)
        FROM Track t
        WHERE t.AlbumId = a.AlbumId) AS Num_canciones
FROM Album a
WHERE a.Title = 'Out Of Time';
