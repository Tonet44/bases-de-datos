#Query11-Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

SELECT *
FROM Customer;

SELECT  c.FirstName, c.LastName, i.Total
FROM invoice AS i JOIN Customer AS c On i.CustomerId = c.CustomerId
WHERE i.Total >10;