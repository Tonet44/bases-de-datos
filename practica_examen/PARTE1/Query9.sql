#Query9-Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas:
#fecha de la factura, nombre completo del cliente, dirección de facturación,
#código postal, país, importe (en este orden).

SELECT *
FROM Customer
where City = "Berlin";


SELECT *
FROM Invoice;

SELECT I.invoiceDate, C.FirstName, C.LastName, I.BillingAddress, I.BillingPostalCode, I.BillingCountry
FROM Invoice AS I JOIN Customer AS C ON I.CustomerId = C.CustomerId
WHERE C.City = "Berlin";