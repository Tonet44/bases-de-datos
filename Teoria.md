Antonio Esteban Lorenzo.
1 DAW.Bases de datos

## TEORIA

## GITLAB
USUARIO Y CONTRASEÑA
* Mi Usuario: Tonet44
* Mi contraseña: **********
* Usuario profe: guyikcgg

Para poder entregar nuestros ejercicios vamos a utilizar GITLAB, para ello hay que aprenderse una serie de comandos para poder descargar y cargar datos al servidor de GIT LAB.

* Si tengo un repositorio en GITLAB y me lo quiero descargar usaremos el comando `git pull`.
* Una vez descargado tenemos que hacer un `git clone`.
* Si queremos subirlo a la plataforma tenemos que hacer un `git push`.
* Si queremos clonar a nuestro repositorio remoto un repositorio de un comañero utilizaremos `git fork`

Para poder descargar y subir cosas debemos de crearnos un `Token`. Un token es una contraseña creada por la aplicación la cual le podemos dar nosotros la vida que queramos al crearlo.
Para poder clonar un repositorio debemos de utilizar esto de la siguiente manera:
* git clone https:\\usuario:token@gitlab.com...........

## CLAVE PRINCIPAL y CLAVE FORANEA

![comandos](imagenes/forenig_key.png)

La clave principal o primary Key suelen tener una llave amarilla en los esquemas, o suelen estar en negrita y subrrayado.
La clave Foranea o Foreing Key suele estar con un rombo rojo o en negrita.

## JOIN

Un `JOIN` se utiliza para combinar filas de dos o más tablas, en función de una columna relacionada entre ellas.

INNER JOIN

Este es el `JOIN` por defecto, solo seleccionará los datos que coincidan con las dos claves escogidas. Por ejemplo en una tabla de películas y otra de género las relacionamos entre ellas, con sus claves nos saldran solo las películas que tengan género, si hay una pelicula que no tiene género no saladrá en la tabla. 

LEFT JOIN

En este caso `LEFT JOIN` seleccionará los datos relacionados entre las dos claves y ademas los no relacionados de la tabla de la izquierda aun que no tenga relación. Por ejemplo en una tabla de películas y otra de género las relacionamos entre ellas, con sus claves nos saldran las películas que tengan género y ademas las películas sin género por que es la tabla que hemos puesto a la izquierda.

RIGHT JOIN

En este caso `RIGHT JOIN` seleccionará los datos relacionados entre las dos claves y ademas los no relacionados de la tabla de la derecha aun que no tenga relación. Por ejemplo en una tabla de películas y otra de género las relacionamos entre ellas, con sus claves nos saldran las películas que tengan género y ademas los géneros que tenga la tabla y que no tengan ninguna película relacionada por que es la tabla que hemos puesto a la derecha.

FULL OUTER JOIN

En este caso `FULL OUTER JOIN` seleccionará los datos relacionados entre las dos claves tengan o no relación, es decir relacionado y no relacionados.




