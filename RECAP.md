# Unidad C0: Recapitulación

Antonio Esteban Lorenzo / 1DAW Bases de datos

En este documento trato de explicar como funciona una base de datos, como nos podemos comunicar con ella, como podemos obter, crear y eliminar datos.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Las bases de datos es donde se guardan una serie de diferentes datos, que se pueden relacionar entre si, y el cual luego nos permite realizar peticiones sobre varios datos de forma rápida.

Definición de base de datos: Conjunto de datos organizado de tal modo que permita obtener con rapidez diversos tipos de información. [[1](#Referencias)].

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? 
Definición de DBMS.

El DBMS es un sofware que gestiona y crea base de datos. 
Permite acceder a la base de datos para poder crear, eliminar consultar y leer datos. 


### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor? 

* Oracle DB (privativo, no libre)
* IMB Db2 (privativo, no libre)
* SQLite (código abierto y libre)
* MariaDB (código abierto y libre)
* SQL Server (privativo, no libre)
* PostgreSQL (código abierto y libre)
* mySQL (código abierto y libre),(cliente-servidor)
* Firebird (código abierto y libre)

Sofware de código abierto y libre FOSS y FLOSS, es el software​ que está licenciado de tal manera que los usuarios pueden estudiar, modificar y mejorar su diseño mediante la disponibilidad de su código fuente. [[2](#Referencias)].

En mi opinión todos estos sistemas utilizan el modelo cliente-servidor, ya que hoy en día parece inpensable que un DBMS no pueda conectarse de forma remota a través de una red para el trabajo del día a día.


## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? 
Porque así podemos trabajar con la base de datos desde cualquier lugar, es decir no hace falta estar en la misma máquina para poder trabajar con ella.
¿Qué ventajas tiene desacoplar al DBMS del cliente? 
El que podamos conectarnos a traves de una red desde varios ordenadores
¿En qué se basa el modelo cliente-servidor? 
En poder conectarte a la base de datos desde cualquier lugar con una red.

* __Cliente__: Hardware o Sofware que realiza las peticiones al servidor
* __Servidor__: Ordenador o sofware que envia las respuestas al cliente
* __Red__: Es el medio para comunicar el cliente-servidor.
* __Puerto de escucha__: Es el puerto por donde se conecta la red a la base de datos
* __Petición__: Las consulas que realiza el cliente
* __Respuesta__: Las respuestas del servidor al cliente

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?
SQL es un lenguaje de manipulación de datos (DML), el cual nos permite comunicarnos con la base de datos y poder realizar consultas, insertar datos, eliminarlos...

### Instrucciones de SQL

#### DDL
Son aquellas utilizadas para la creación de una base de datos y todos sus componentes: tablas, índices, relaciones, disparadores (triggers), procedimientos almacenados, etc
#### DML
Son aquellas utilizadas para insertar, borrar, modificar y consultar los datos de una base de datos.
#### DCL
Controla el acceso de los datos.
#### TCL
Se usa para la transacción de datos.

![comandos](imagenes/comandos_sql.PNG)
[[5](#Referencias)].


## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?
Una base de datos relacional es aquella que permite consultar datos de distintas tablas a la vez y relacionarlas entre si.
La ventaja que tiene que puede almacenar mucha información y de distinto tipo en diferentes tablas y luego poder relacionarlas de manera rápida.

* __Relación (tabla)__: hace coincidir los datos de los campos clave.
* __Atributo/Campo (columna)__: valores únicos (datos) que proporcionan la estructura según la cual se descomponen las filas. [[3](#Referencias)].
* __Registro/Tupla (fila)__: representa un objeto único de datos implícitamente estructurados en una tabla. [[4](#Referencias)].

## Esquema/Resumen
![esquema](imagenes/esquema.jpeg)
![gráfico](imagenes/Grafico.png)

## Referencias 

[1] https://www.arimetrics.com/glosario-digital/base-de-datos

[2]https://es.wikipedia.org/wiki/GNU_General_Public_License

[3]https://es.wikipedia.org/wiki/Columna_(base_de_datos)#:~:text=En%20el%20contexto%20de%20una,o%20tuplas%20de%20una%20tabla.

[4]https://es.wikipedia.org/wiki/Registro_(base_de_datos)#:~:text=En%20informática%2C%20o%20concretamente%20en,columnas%20(campos%20o%20atributos).

[5] https://jadcode.wordpress.com/2018/03/08 sql-comandos-dcl-y-tcl-parte-3/

* https://hoplasoftware.com/mysql-sistema-de-gestion-de-bases-de-datos-relacionales/#:~:text=Características%20de%20MySQL&text=Arquitectura%20Cliente%20y%20Servidor%3A%20MySQL,diferenciada%20para%20un%20mejor%20rendimiento.

* https://www.youtube.com/watch?v=y6XdzBNC0_0
